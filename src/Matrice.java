package src;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Matrice {
    List<PairVal> jeu = new ArrayList<>();

    public List<PairVal> getJeu() {
        return jeu;
    }

    public void add(int x, int y, int val){

        this.jeu.add(new PairVal(x,y,val));
    }

    // decal int x int y
    public List<PairVal> decal(int x, int y) {
        List<PairVal> listNewPair = new ArrayList<>();
        for (PairVal pair : jeu) {
            PairVal newPair = new PairVal(pair.getX()+x, pair.getY()+y, 1);
            listNewPair.add(newPair);
        }
        return listNewPair;
    }

    // somme Matrice m
    // add the matrice 'this' and the matrice 'm'
    public void somme(Matrice m) {
        for (PairVal pairM : m.getJeu()) {
            if (this.jeu.contains(pairM)){ // if 'pairM' is in 'this.jeu' we add his 'val' to the val of is corresponding in 'this.jeu'
                PairVal pair = this.jeu.get(this.jeu.indexOf(pairM));
                pair.val += pairM.getVal();
            }
            else { // else we create 'pairM' in 'this.jeu' with the value he has in m
                this.jeu.add(pairM);
            }
        }
    }
    // mult mutiplie tout les val des elem a
    public void mult(int a) {
        // TODO j'ai pas trop compris
    }
    // filtre garde seulement ceux avec la val v
    public void filtre(int v) {
        // TODO
    }

    public String toString() {
        String res = "";
        int x = 0;
        int y = 0;
        for (PairVal pair : this.jeu) {
            int espaceEntreCelluleX = pair.getX()-x;
            if (espaceEntreCelluleX > 0) {
                res += "\n";
                for (int i=0; i<espaceEntreCelluleX-1; i++) res += ".\n";
                x+=espaceEntreCelluleX;
                y=0;
            }

            int espaceEntreCelluleY = pair.getY()-y;
            if (espaceEntreCelluleY > 0) {
                for (int i=0; i<espaceEntreCelluleY; i++) res += ".";
                y+=espaceEntreCelluleY+1;
            }
            res = res + '*';
            y++;
        }
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrice matrice = (Matrice) o;
        return Objects.equals(jeu, matrice.jeu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jeu);
    }



}


