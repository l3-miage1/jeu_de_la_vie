package src;

import java.util.Objects;

public class PairVal {
    int x;
    int y;
    int val;

    public PairVal(int x, int y, int val) {
        this.x = x;
        this.y = y;
        this.val = val;
    }

    public int getX() {return x;}
    public int getY() {return y;}
    public int getVal() {return val;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairVal pairVal = (PairVal) o;
        return x == pairVal.x && y == pairVal.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "PairVal{" +
                "x=" + x +
                ", y=" + y +
                ", val=" + val +
                '}';
    }
}

