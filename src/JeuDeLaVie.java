package src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class JeuDeLaVie {

    Matrice jeu = new Matrice();
    Matrice before = null ;

    public JeuDeLaVie(String chemin) {
        try
        {
            // Le fichier d'entrée
            File file = new File(chemin);
            // Créer l'objet File Reader
            FileReader fr = new FileReader(file);
            // Créer l'objet BufferedReader
            BufferedReader br = new BufferedReader(fr);
            String line;
            int x = 0;
            while((line = br.readLine()) != null)
            {
                if (line.charAt(0) != ('#')) {
                    for (int y=0; y<line.length(); y++){
                        if (line.charAt(y) == ('*')) {
                            this.jeu.add(x, y, 1);
                        }
                    }
                    x++;
                }
            }
            fr.close();

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public boolean endGame(){
        return jeu.equals(before);
    }

    public void quaranteDeux(){

        while(!endGame()){



            // TimeUnit.SECONDS.sleep(1);
        }
    }

    @Override
    public String toString() {
        return jeu.toString();
    }
}
